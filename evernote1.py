class circbuffer():
    #initialization
    def __init__(self,size):
        self.maximum=size
        self.data=[]
        self.current=0
    #appending when the buffer is not full
    def append(self,x):
        if len(self.data)==self.maximum:
            self.current=0
            self.data[self.current]=x
            self.current=(self.current+1)%self.maximum
            self.__class__=bufferfull
        else:
            self.data.append(x)
    def remove(self):
        if self.data:
            self.data.pop(0)
    def cget(self):
        return self.data

class bufferfull:
    def append(self,x):
        if len(self.data)<self.maximum:
            self.data.insert(self.current, x)
        else:
            self.data[self.current]=x
        self.current=(self.current+1)%self.maximum
    def remove(self):
        if self.data:
            if self.current>len(self.data):
                self.current=0
            self.data.pop(self.current)
    def cget(self):
        return self.data[self.current:]+self.data[:self.current]

def evernote1(path):
    f = open(path)

    n = 0                    # cnt of line of file read
    a_cnt = 0                # cnt of append with command 'A'
    tsize = 0                # total char size
    cbuf = circbuffer(50000) # circular buffer of size 50000
    
    for line in f:
        if tsize > 20000000:
            print "Range Over"
            return            
        if n > 50000:
            print "Range Over"
            return
        if n == 0:
            nmax = int(line)    # No of command
        elif n < nmax:
            if a_cnt == 0:              # normal mode 
                l = line.split(' ')
                if l[0] == 'A':         
                    a_cnt = int(l[1])   # 'A' command to data append mode
                elif l[0] == 'R':
                    for n in range(int(l[1])):  # remove l[1] times
                        cbuf.remove()
                else:                    # 
                    cmd = l[0].split('\n', 1)
                    if cmd[0] == 'L':  
                        ans = cbuf.cget()     # get data
                        print '\n'.join(map(str, ans))   # formatted print
                    elif cmd[0] == 'Q':
                        return                    
            else:                           # data append mode
                l = line.split('\n', 1)      
                cbuf.append(l[0])   
                tsize += len(l[0])          # update char cnt
                a_cnt -= 1                  # decrease append cnt
        n += 1                              # increase line cnt
            
evernote1('evernote1.txt')


