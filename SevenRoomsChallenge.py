from random import randint, seed
from math import factorial, pow

def ping_hit(p):  # return hit, one of 'flat', 'slice', 'topspin', 'unreturnable'
    hits = ['flat', 'slice', 'topspin', 'unreturnable']
    Bruce = [47, 72, 97, 100]
    Serena = [10, 30, 96, 100]
    Jean = [70, 80, 95, 100]
    players=[Bruce, Serena, Jean]

    r = randint(0, 100)   # random generator
    hit = players[p]
    n = 0
    for h in hit:
        if r <= h:
            return hits[n] 
        n += 1

def ping_ret(p, h):  # return pass(1)/fail(0) of return to hit input h
    rets = ['flat', 'slice', 'topspin']
    Bruce = [80, 45, 75] 
    Serena = [65, 50, 85]  
    Jean = [90, 25, 85] 
    players=[Bruce, Serena, Jean]

    if h is 'unreturnable':
        return 0
    else:
        r = randint(0, 100)    # random generator
        if r <= players[p][rets.index(h)]:
            return 1           # success to return
        else:
            return 0           # fail to return

def pingpong(players):
    score = [0, 0]
    ping = 0
    players_name=['Bruce', 'Serena', 'Jean']

    while max(score) < 21:
        p1 = (ping // 5) % 2   # index of first player(0 or 1) (service seq)
        p2 = (p1 + 1) % 2      # another index (0 or 1) of player
        pl1 = players[p1]      # player number of p1
        pl2 = players[p2]      # player number of p2

        while True:
            h1 = ping_hit(pl1)                    # hit by first player
            if ping_ret(pl2, h1) == 0:            
                score[p1] += 1
                break
            h2 = ping_hit(pl2)                    # hit by second player
            if ping_ret(pl1, h2) == 0:
                score[p2] += 1
                break
        ping += 1                                 # for serve cnt
        #print players_name[players[0]], players_name[players[1]],score

    if score[0] > score[1]:
        #print  players_name[players[0]], 'wins', score
        return 1                      # first player wins
    else:
        #print  players_name[players[1]], 'wins', score
        return 0                      # second player wins

def calc_win(n, m):                               # calculate probability
    hits=[[47, 25, 25, 3], [10, 20, 66, 4], [70, 10, 15, 5]]  # percent of hit
    rets=[[80, 45, 75, 0], [65, 50, 85, 0], [90, 25, 85, 0]]  # percent of return
                                # 0(%) for 'unreturnable'

    hit = hits[n]
    ret = rets[m]
    pro1 = 0.
    for l in range(4):
        pro1 += (hit[l] * (100.-ret[l]))
    pro1 /= 10000.         # 1 time win probability for first player
        
    hit = hits[m]
    ret = rets[n]
    pro2 = 0.
    for l in range(4):
        pro2 += (hit[l] * (100.-ret[l]))
    pro2 /= 10000.         # 1 time win probability for second player

                           # win probability after rally, infinite series
    draw = 2. * (1. - (1. - pro1) * (1 - pro2))   # common ratio
    win1 = (2. - pro2) * pro1 / draw 
    win2 = (2. - pro1) * pro2 / draw 

                            # win probability of (21 + n) binomial distribution
    twin1 = 0. 
    twin2 = 0.
    for l in range(20):
        combi = 1. * factorial(21+l) / factorial(l) / factorial(21)
        combi1 = combi * pow(win1, 21) * pow(win2, l)
        combi2 = combi * pow(win2, 21) * pow(win1, l)
        twin1 += combi1
        twin2 += combi2
        
    #print pro1, pro2, pro1/(pro1+pro2), win1, win2, win1/(win1+win2), win1 + win2
    #print pro1/(pro1+pro2), win1/(win1+win2), twin1/(twin1+twin2)
    return twin1/(twin1+twin2)
    
players_name=['Bruce', 'Serena', 'Jean']

seed()
total = 1000;
for n in range(3):
    for m in range(3):
        if n != m:
            winp = calc_win(n, m)   # calculate theoretical probability

            result = 0.
            match = [n, m]
            for nn in range(total):   # 1000 tries
                result += pingpong(match)
            print players_name[n], players_name[m], result/total, winp
