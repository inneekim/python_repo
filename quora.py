import math

def quora(path):
    quora_data = open(path)
    tlist = []
    qlist = []
    nlist = []

    n = 0                       # cnt of read line
    for qline in quora_data:    # Generate Lists
        qq = qline.split(' ')   # split with space
        if n == 0:              # T,Q,N
            T = int(qq[0])
            Q = int(qq[1])
            N = int(qq[2])
            if T == 0 or T > 10000 or Q == 0 or Q > 1000 or N == 0 or N > 10000:
                print 'Range Over !!!'
                return
        elif n <= T:            # T List
            tlist.append((float(qq[1]), float(qq[2])))
        elif n <= T+Q:          # Q List
            qlist0 = []
            for m in range(0, int(qq[1])):
                qlist0.append(int(qq[m+2]))                     
            qlist.append(qlist0)
        else:                   # N List
            nlist.append([qq[0], int(qq[1]), float(qq[2]), float(qq[3])])
                   
        n += 1

    #print T, Q, N
    #print tlist
    #print qlist
    #print nlist

    for nn in nlist:             # N Questions
        dist = []                # distance list
        for t in tlist:
            xdif = t[0]-nn[2]
            ydif = t[1]-nn[3]
            distance = math.sqrt(xdif*xdif + ydif*ydif)
            dist.append(distance)
        dorder = sorted(range(len(dist)),key=lambda x:dist[x])  # sorted order of distance

        ans = []

        if nn[0] == 't':        # get the nearest points of No. of nn[0] 
            ans.extend(dorder[0:nn[1]])
            
        elif nn[0] == 'q':
            n = 0               # cnt of ans
            m = 0               # cnt of dorder
            m_max = len(dorder)
            while n < nn[1] and m < m_max:
                key = dorder[m]
                for q in qlist:   # scan qlist
                    if key in q:  # if key exists in q
                        n += 1    # increase cnt of ans
                        ll = qlist.index(q)   # get index of q in qlist
                        ans.append(ll)        # append the index
                        qlist[ll] = []        # remove q in the qlist
                m += 1

        print ' '.join(map(str, ans))         # formatted print
                    
quora('quora.txt')

    
