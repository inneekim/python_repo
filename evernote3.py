import numpy

def evernote3(path):
    f = open(path)

    nums = []   # set for numbers
    n = 0
    for line in f:
        if n > 1000:
            print "Range Over"
            return
        if n == 0:
            nmax = int(line)
            if nmax > 1000:
                print "Range Over"
                return
        elif n <= nmax:
            nums.append(int(line))
            
        n += 1        

    tot = numpy.int64(1)  # 64bit int
    for num in nums:      # total product
        tot *= num

    ans = []
    for num in nums:
        ans.append(tot/num)  
        
    print ans
        
evernote3('evernote3.txt')
