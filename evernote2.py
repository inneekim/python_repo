def evernote2(path):
    f = open(path)

    wrd = []   # set for words
    frq = []   # cnt for each word 
    n = 0
    for line in f:
        if n >= 300000:
            print "Range Over"
            return
        if n == 0:
            nmax = int(line)
            if nmax >= 300000:
                print "Range Over"
                return
        elif n <= nmax:
            if len(line) > 25:
                print "Range Over"
                return                
            w = line.split('\n', 1)[0]   # split word
            if w in wrd:                 # if exing word
                frq[wrd.index(w)] += 1   # increase cnt
            else:
                wrd.append(w)            # if new word
                frq.append(1)            # cnt = 1
        else:
            dorder = sorted(range(len(frq)),key=lambda x:frq[x], reverse=True)
            ans = []
            for d in dorder[0:int(line)]:  # k most frequent words
                ans.append(wrd[d])            
            
        n += 1        
    
    #print wrd
    #print frq
    #print dorder
    print ans
        
evernote2('evernote2.txt')
